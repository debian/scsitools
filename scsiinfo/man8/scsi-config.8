.TH scsi-config 8 "24 August 1997" "scsiinfo 1.7" "Scsiinfo User's Guide"
.SH NAME
scsi-config \- query information from a scsi device with a nice user interface
.SH SYNOPSIS
.BI "scsi-config [" device ]
.SH DESCRIPTION
.B scsi-config
queries information from an scsi target with a nice Tcl/Tk user interface. If you do not specify 
a device to query,
.B scsi-config
calculates a list of available devices and prompts it to you.

By the nature of a graphical user interface, most things will explain them self. Basically,
.B scsi-config
shows a list of buttons for certain mode pages which you may press. Those buttons which
you can press and the text windows with white backgrounds can be modified by you and the
modifications send back to the device.

In the main window there is a button to instruct the device to save the data in some
non volatile memory (if it supports it). Note that this will instruct the device to
save the
.B "Read-Write Error Recovery Page" ", "
.B "Disconnect-Reconnect Page" ", "
.B "Format Device Page" ", "
.B "Caching Page" ", and "
.B "Control Mode Page" in its NVRAM. Usually saving even a single of those should write
them all to the NVRAM, but you never know.

You can query the current, the factory default and the values in the NVRAM (which may
not be the current parameters) from the device.

Not all combinations of button toggles or all values are valid. In general, try to set them
and see which values the drive accepts.
.B scsi-config
rereads the device configuration immediately, s.t. you see which values where accepted.

Also note that some disk drives are notched, and that those have an active notch (shown in the
main window) to which all your settings apply (at least those of notched pages, which are
also marked in slate gray). You can select the active notch to which your settings apply
(and to which the values refer) in the
.BR "Notch Page" .

For those devices which do not feature an NVRAM (generally removable media devices) and
as a kind of backup, you can save the current settings to a file. Actually the file will
be a
.I /bin/sh
script making the necessary
.BR scsiinfo (8)
calls to set the saved parameters when executed.

There is also a nice Overview button which will query many details about the disk geometry
and draw them in a nice picture. This looks esp. nice for drives with many notches, that is
different regions on the disk with different tracks per sector settings. It is also very
useful for notched drives as you can immediately select the mode pages for each notch.

.SH SOME USAGE GUIDELINES
.TP
.B 1. General Warning
Generally, do not modify settings you don't understand. It is useful to know the SCSI-II
specs mentioned below. Some setting may render the device unusable or even damage it. Usually
a power cycle resets the state (if you do not save the weird settings in the NVRAM). Some
settings affecting the assignment of logical sectors will render the disk unusable until the
next low level format.
.TP
.B 2. On Write Caching
As an old warning, this does also mean you should not generally switch the write cache on.
At least on those drives where you have a choice at all. Reasons are twofold:
.RS
.TP
.B a)
It is a priori unclear when the drive will actually perform the writes. This is a bad thing
when considering shutdown of your machine. On the other hand, it seems sensible to assume that
the drive will immediately write it's cache to disk when it is idle (after all file systems
are unmounted) and due to the size of the on disk cache this will usually only need a few
seconds after the shutdown (but the drive lamp will usually not glow, as it is mostly connected
to the host adapter (if you have a lamp at all) and it is not participating).

There is a SCSI command to flush the caches. Linux could call it prior to shutting down,
spinning a disk down. Due to my knowledge this is not yet done though.
.TP
.B b)
As the writes are performed asynchronously, errors are reported asynchronously. The disk might
return an error at some simple read instruction related to a write which was acknowledged OK
several transactions ago. This generally confuses things and makes interpreting errors very
difficult. Some devices are known/said to not report write errors in this mode of operation at all.

Just imagine that at the point where a file system is unmounted, or a new removable media
is detected it could tell: Oops, BTW, there was some write error ago although I told you it was OK
already.
.RE

.IP
Thus, when you run a disk in write cache mode, keep it in mind when weird error messages occur
and give the disk time to flush it's buffers at shutdown. Generally it would be good if you
knew more vendor specific details on how the disk operates in the write cache mode.

.TP
.B 3. Reassigning Bad Blocks Automatically
One of the nice features of SCSI disk is that they allow to remap bad blocks automatically as they
are detected without any user intervention. However, you actually have to enable this feature!
It turned out that you can not generally assume a disk in this mode. To enable this mode or check
the settings, proceed as follows:
.RS
.TP
.B a)
Go to the
.BR "Read-Write Error Recovery Page" .
.BR AWRE " (Automatic Write Reallocation Enable) and " ARRE " (Automatic Read Reallocation Enable)"
buttons enable the automatic reallocation.

In the same window, you can select the maximal retries performed. EER allows the disk
to do some Early Error Recovery which is fast (but might misdetect or miscorrect data).
A selected DCR button (Disable CoRrection Codes) disallows the disk to use any error correction
codes at all (thus the drive will have to retry until it performs an error free read).

Usually a sector will be reallocated after even a single read retry or the given number of failed
write retries. When the sector cannot be recovered, it is reallocated but the data is lost and
an error is signalled.

The other buttons there apply to error reporting as well. TB Transmits the bad Block together with
the error, RC Reads Continuous, that is, does not pause a read operation while retrying or
using error code calculations to recover a bad block (thus may return bad data). PER lets the
disk report even recovered errors (Post ERror), DTE (Disable Transfer on Error) even breaks a
running data transmission when an error is detected.

.TP
.B b)
Even when the reallocation is enabled, the disk must actually have some reserved areas where to
remap the bad blocks. The
.B Format Page
controls this. Either a given number of Alternate Sectors Per LUN is set aside for the whole
disk or a given number of tracks is defined to be a
.I zone
and for each zone a number of sectors or tracks is put aside.
These alternate data areas are where bad sectors are remapped.

Note that this page is very likely to apply only to the current notch on a notched disk device.

If there are no, or not many alternate sectors reserved on your disk, you must change these
settings.

I found that those disks which allow to modify these settings are very often set to no reserved
sectors at all by the vendors, as this increases the disks capacity. For the sake of stability,
you should really modify these settings.

If you decide to modify the number of alternate sectors, you must
.RS
.TP
.B i)
Save the parameters to the NVRAM of the disk.
.TP
.B ii)
Low-level format the disk drive (and not erase the NVRAM during this operation).
.RE
.IP
to make the changes be effective.

.TP
.B c)
Esp. when you set the disk to not report recovered errors (or when it is in a write cache mode or
something), and just as a general guideline, keep an eye on the grown defects list where the
disk will report all the remappings which took place as your disk ages.

The old adventurers guide line applies: Save (Backup) Early, Save (Backup) Often. Find the right
time to replace your disk avoiding too much work recovering your data.
.RE

.SH BUGS
.B scsi-config
could be enhanced by making better use of Tcl/Tk. I learned much about it during my work on
.BR tk_scsiformat (8)
and their would be much room for fixes and enhances. On other hand, it fulfils it's purpose as
it is quite nicely already.

As
.B scsi-config
makes heavy use of
.BR scsiinfo (8)
all it's bugs (esp. on defect reading)
apply.

.SH FILES
.IR /usr/lib/scsi/cache ,
.IR /usr/lib/scsi/control ,
.IR /usr/lib/scsi/disconnect ,
.IR /usr/lib/scsi/error ,
.IR /usr/lib/scsi/format ,
.IR /usr/lib/scsi/generic ,
.IR /usr/lib/scsi/inquiry ,
.IR /usr/lib/scsi/notch ,
.IR /usr/lib/scsi/overview ,
.IR /usr/lib/scsi/peripheral ,
.IR /usr/lib/scsi/rigid ,
.IR /usr/lib/scsi/save-changes ,
.IR /usr/lib/scsi/save-file ,
.IR /usr/lib/scsi/tworands ,
.IR /usr/lib/scsi/verify " (Tcl/Tk subroutines used by scsi-config)."
.br
.I /dev/sd*
.br
.I /dev/sg*
.br
.I /dev/scd*
.br
.I /dev/st*
.br
.I /dev/nst*
.br
.I /dev/rmt*
.br
.I /dev/nrmt*

.SH SEE ALSO
.BR scsiinfo (8),
.BR scsiformat (8),
.BR tk_scsiformat (8),
.BR fdisk (8),
.BR sd (4),

.B Draft proposed
.br
.B American National Standard
.br
.B for information systems

.B SMALL COMPUTER SYSTEM INTERFACE - 2
.br
.B (SCSI-2)

.B MARCH 9, 1990

.SH AUTHORS
Eric Youngdale.
.br
Michael Weller <eowmob@exp-math.uni-essen.de>, Versions 1.5 & 1.7
