/* Simply utility to get two random numbers which is only
   supported by extended tcl, s.t. we need a script for it */

#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>

int main(int argc) {
    struct timeval tv;

    if (argc != 1) {
	fputs("Usage: tworands\n"
	      "\tprint two random integers in range 1..25\n", stderr);
	return 2;
    }
    gettimeofday(&tv, NULL);
    srand((unsigned)(tv.tv_sec + tv.tv_usec));
    srand((unsigned)rand());
    srand((unsigned)rand());
    printf("%d %d\n",
	1 + (int)(24.0 * rand() / (RAND_MAX + 1.0)),
	1 + (int)(24.0 * rand() / (RAND_MAX + 1.0))
    );
    return 0;
}
