/* 
 * Program srawread.c
 *
 * A program to benchmark raw scsi I/O performance under linux
 *
 * Eric Youngdale  -  9/13/93
 *
 * This program was written because of all of the whining on the SCSI
 * channel about poor performance of the scsi drivers.  This program basically
 * reads the specified scsi device and measures the throughput.  Note that
 * the filesystem *AND* the buffer cache are bypassed by this code, this
 * program was designed to benchmark the naked scsi drivers by themselves
 * without the need to account for the overhead of any other portion of the
 * kernel.
 *
 * This program does a series of reads of the disk, of consecutive
 * areas on the disk.  The device is first queried to determine the
 * sector size for the device, and then the series of reads is begun.
 * About 5.0 Mb is read from the device, and then the performance numbers
 * are reported.  Note that since the buffer cache is completely bypassed,
 * there is no need to be concerned about cache hits or anything.
 *
 * With my Texel double speed cdrom, I obtain about 303Kb/sec with a 4Kb
 * blocksize, and this is almost exactly the specified data rate for
 * the device.
 *
 * For my Toshiba hard disk, I obtain the following numbers (aha1542B):
 *
 * BKSZ(bytes) 	Datarate(b/sec)
 * 4096 	  673684
 * 8192 	  960600
 * 16384	 1288490
 * 32768	 1587832
 * 49152 	 1755428
 * 65536	 1761001
 * 
 * I should point out that
 * with normal sequential file reads through a filesystem, that normally
 * there will be a read-ahead of approximately 16 sectors or 8192 bytes
 * in addition to the data requested by the read.  For a 4Kb blocksize
 * filesystem, typically 12Kb are requested at one time, so
 * theoretically one can get something quite larger.  I have experimented
 * with block devices that have larger block sizes, and I have seen data
 * rates of about 1.1Mb/sec with the same disk drive using a blocksize of
 * 4Kb with ext2 and using iozone to measure performance, so I know that
 * this is real.
 *
 * The Toshiba disk has a rotational rate of 3600 RPM, so the
 * peak I/O rate corresponds to something like 57 sectors per
 * revolution.  There are 85 sectors per track on this drive
 * which would correspond to a maximum theoretical throughput
 * of 2.6Mb/sec.  Note that the Adaptec has bus on/off times that
 * it uses to ensure that it does not hog the bus.  The current
 * settings in the linux kernel are 7 microseconds on and 5
 * microseconds off.  When you multiply the theoretical maximum
 * throughput by (7/(7+5)) you get about 1.5Mb/sec.  I am not sure
 * how to factor this into the equation because there is undoubtably
 * caching somewhere.  I suppose that it is safe to say that the
 * theoretical maximum speed you will ever observe is somewhere
 * between the two numbers.
 *
 * To use this you simply compile and then use the following command:
 *
 * ./rawread /dev/sda
 *
 * to perform the benchmark.  It should do only reads, so it should be
 * safe, but you will probably need root privileges, and if this screws
 * up something for you do not come running to me.
 *
 * 
 * Changes from Harald Koenig  -  11/25/93
 *
 * - added READ_10 support for > 1GB discs
 * - using gettimeofday() for timing
 * - two new command line parameters (argv[2] and argv[3]):
 *   - argv[2] (bstart) : starting block to check different zones on ZBR discs
 *   - argv[3] (bstep)  : factor for sequential stepping, default 1
 *                        use 0 for reading always the same blocks (from cache)
 *
 */

#define USE_READ_10
#undef  SET_FUA  /* "force unit access" for READ(10): don't read from cache but from media */
#define LESS_VERBOSE_OUTPUT
#define USE_GETTIMEOFDAY /* please, always! gettimeofday(2) is much more accurate */


#include <stdio.h>
#include <unistd.h>
#include <sys/times.h>
#include <linux/hdreg.h>

FILE *infile, *ptable;
unsigned char buffer[2*64*1024];


int read_size=32768;

static double
  time_so_far()
{
#ifdef USE_GETTIMEOFDAY
#include <sys/time.h>
  struct timeval t1;

  gettimeofday(&t1,NULL);
  return (t1.tv_sec + t1.tv_usec*1e-6);
#else /* USE_GETTIMEOFDAY */
  clock_t        val;
  struct tms tms;


  if ((val = times(&tms)) == -1)
    perror("times");

  return ((double) val) / ((double) CLK_TCK);
#endif /* USE_GETTIMEOFDAY */
}


int main( int argc, char *argv[] )
{
  int b,bstart=0,bstep=1;
  int status, i;
  unsigned char *cmd;
  int capacity, sectorsize;
  int this_count, block;
  int rate;
  double starttime, endtime;

  if (argc==1) { 
    printf("usage: srawread scsi-device [ bstart [ bstop ] ]\n");
    exit(0);
  }

  infile = fopen( argv[1], "r" );
  if(!infile) exit(0);

  if (argc>2) bstart = atoi(argv[2]);
  if (argc>3) bstep  = atoi(argv[2]);

  for (i=0; i<10*1024; i++)
  {
    buffer[i] = 0;
  }
  
  *( (int *)  buffer )		= 0;	/* length of input data */
  *( ((int *) buffer) + 1 )	= read_size;	/* length of output buffer */

  cmd = (char *) ( ((int *) buffer) + 2 );
  
  cmd[0] = 0x25;			/* INQUIRY */
  cmd[1] = 0x00;			/* lun=0, evpd=0 */
  cmd[2] = 0x00;			/* page code = 0 */
  cmd[3] = 0x00;			/* (reserved) */
  cmd[4] = 0x00;			/* allocation length */
  cmd[5] = 0x00;			/* control */

  status = ioctl( fileno(infile), 1 /* SCSI_IOCTL_SEND_COMMAND */, buffer );

  capacity = (buffer[8] << 24) | (buffer[9] << 16)  | (buffer[10] << 8) | buffer[11];
  sectorsize = (buffer[12] << 24) | (buffer[13] << 16)  | (buffer[14] << 8) | buffer[15];
  printf("Size %d  sectorsize %d\n", capacity * (sectorsize >> 9), sectorsize);


  do{
	  for (i=0; i<10*1024; i++)
	  {
		  buffer[i] = 0;
	  }
	  
	  block = 0;
	  this_count = read_size / sectorsize;
	  starttime = time_so_far();
	  do{
		  *( (int *)  buffer )		= 0;	/* length of input data */
		  *( ((int *) buffer) + 1 )	= read_size;	/* length of output buffer */
		  
		  cmd = (char *) ( ((int *) buffer) + 2 );
		  
		  b = bstart + bstep * block;
#ifdef USE_READ_10
		  cmd[0] = 0x28;			/* read_10 */
		  cmd[1] = 0x00;
#ifdef SET_FUA
		  cmd[1] |= 0x08;
#endif /* SET_FUA */
		  cmd[2] = (b >> 24) & 0xff;
		  cmd[3] = (b >> 16) & 0xff;
		  cmd[4] = (b >>  8) & 0xff;
		  cmd[5] =  b        & 0xff;
		  cmd[6] = 0;
		  cmd[7] = (this_count >> 8) & 0xff; /* transfer length */
		  cmd[8] =  this_count       & 0xff;
		  cmd[9] = 0x00; /* control */
#else /* USE_READ_10 */
		  cmd[0] = 0x08;			/* read_6 */
		  cmd[1] = (b >> 16) & 0x1f;
		  cmd[2] = (b >> 8) & 0xff;
		  cmd[3] = b & 0xff;
		  cmd[4] = this_count;
		  cmd[5] = 0x00;
#endif /* USE_READ_10 */
		  
		  status = ioctl( fileno(infile), 3 /* SCSI_IOCTL_BENCHMARK_COMMAND */, buffer );
		  if(status) fprintf(stderr,"%x ", status);
		  block += this_count;
	  } while(block < (10000 / (sectorsize >> 9)));
	  endtime = time_so_far() - starttime;
	  rate = (block * sectorsize) / endtime;
#ifdef LESS_VERBOSE_OUTPUT
	  printf("%6d   %10.4f  %6d    %8d \n",
		 read_size, endtime, block, rate);
#else /* LESS_VERBOSE_OUTPUT */
	  printf("Blocksize %d, time elapsed %1.4f seconds, %d blocks.  Throughput = %d bytes/sec\n",
		 read_size, endtime, block, rate);
#endif /* LESS_VERBOSE_OUTPUT */

	  read_size += 4096;
  } while(read_size <= 2*64*1024);
	  
  return 0;
}

/*  end  */

